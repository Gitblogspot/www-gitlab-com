---
layout: markdown_page
title: "Category Direction - Logging"
description: "A fundamental requirement for running applications is to have a centralized location to manage and review the logs. This is GitLab’s direction on where we are headed with Logging."
canonical_path: "/direction/monitor/apm/logging/"
---

- TOC
{:toc}

## Overview

A fundamental requirement for running and operating applications is to have a centralized location to manage and review the logs. While manually reviewing logs could work when you have just a single node application, once the deployment scales beyond one, you need a solution that can aggregate and centralize your logs for review. Using such a system, users can quickly search through a list of logs that originate from multiple pods and containers to understand and debug issues.

## Current status

We previously launched a solution that enabled users to easily install the ELK stack to aggregate and manage their application logs. GitLab users could directly search for relevant logs in the GitLab UI. 

However, since we deprecated GitLab’s certificate-based integration with Kubernetes clusters, [GitLab Managed Apps is also deprecated](https://docs.gitlab.com/ee/user/clusters/applications.html). Since the ELK stack was a GitLab Managed app, we currently don’t have a recommended solution for Logging within GitLab.

## What's Next & Why

With the [acquisition of Opstrace](link), we intend to add logging as part of GitLab’s observability solution. Currently, we are focused on the product integration between GitLab and Opstrace and will not immediately add logging to Opstrace.
