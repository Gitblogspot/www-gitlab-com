---
layout: handbook-page-toc
title: OKR Management Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## OKR Management Single-Engineer Group

The OKR Management SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/).

The goal of this SEG is to develop Objective, Key, Results (OKR) functionality within GitLab.

Our philosophy is to create an MVP that is:

* Very loose and un-opinionated to accommodate different styles of OKRs across companies, or departments within the same company
* Separate from planning and project management features, but optional linkage
* Accommodating of non-product development persona use from customer/user organizations



